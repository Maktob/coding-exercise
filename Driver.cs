﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriverLogApplication
{

    //Created Driver Class to store necessary properties
    public class Driver
    {
        public string DriverName { get; set; }
        public string Trip { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime StopTime { get; set; }
        public double  MilesDriven { get; set; }

        public TimeSpan TotalHours { get; set; }
        public int AverageSpeed { get; set; }
        public  List<string> TotalTrips { get; set; }

        public void SeparateTripDetails(Driver driver)
        {
            string[] tripdetails = driver.Trip.Split(' ');
            StartTime = Convert.ToDateTime(tripdetails[1]);
            StopTime = Convert.ToDateTime(tripdetails[2]);
            MilesDriven = Math.Round(Convert.ToDouble(tripdetails[3]));
            TotalHours = StopTime - StartTime;
        }

        
    }
}
