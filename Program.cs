using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DriverLogApplication
{
    public class Program
    {
        static void Main(string[] args)
        {
            string UserInput = "";

            //Initialized a list of drivers 
            List<Driver> DriverList = new List<Driver>();

            //initialized a class of driver
            Driver NewDriver = new Driver();

            //this is store trips for drivers that have more than one trip
            NewDriver.TotalTrips = new List<string>();

            //accept driver name from user
            Console.WriteLine("Please enter a driver name:");
            NewDriver.DriverName= Console.ReadLine();

            //accept trip details from user
            Console.WriteLine("Please enter Trip(e.g sola 12:05 14:06 134.2 :");
            NewDriver.Trip = Console.ReadLine();

            //add the driver object created to a list
            DriverList.Add(NewDriver);
            do
            {
                
                    Console.WriteLine("Do you want to enter another Driver Name(Yes/No):");
                    UserInput = Console.ReadLine();
                
                //if a user decides to enter another record for a driver
                if (UserInput.ToLower() == "yes")
                {
                    Driver NewDriverTwo = new Driver();
                    NewDriverTwo.TotalTrips = new List<string>();
                    Console.WriteLine("Please enter a driver name:");
                    NewDriverTwo.DriverName = Console.ReadLine();
                    Console.WriteLine("Please enter Trip(e.g sola 12:05 14:06 134.2 :");
                    NewDriverTwo.Trip = Console.ReadLine();

                    //check if the name entered already exists in our driverlist

                    //if the driver does not exist based on the name add him to the driverlist
                    if (DriverList.Count == 0 || DriverList.Any(item => item.DriverName.ToLower() != NewDriverTwo.DriverName.ToLower()))
                    {

                        DriverList.Add(NewDriverTwo);
                    }

                    //if the driver name already exists update his Totaltrips to reflect the one recently added
                    else
                    {
                        foreach (var m in DriverList.Where(item => item.DriverName.ToLower() == NewDriverTwo.DriverName.ToLower()).ToList())
                        {

                            m.TotalTrips.Add(NewDriverTwo.Trip);

                        }
                    }
                    Console.WriteLine("Do you want to enter another Driver Name:");
                    UserInput = Console.ReadLine();
                    


                }
                if (UserInput.ToLower() == "no")
                {
                    //loop through the driverlist
                   
                      foreach(Driver name in DriverList)
                    {
                        //passed the driver object into a class method to extract details
                         name.SeparateTripDetails(name);

                        //if the driver has only one trip detail
                        if (name.TotalTrips.Count == 0)
                        {
                          
                            name.TotalHours = name.StopTime - name.StartTime;
                            name.AverageSpeed = Convert.ToInt32(name.MilesDriven / (name.TotalHours.Hours));
                        }

                        //if the driver has more than one trip details,
                        else if (name.TotalTrips.Count > 0)
                        {
                            
                            name.TotalHours = name.StopTime - name.StartTime;
                            foreach (var m in name.TotalTrips)
                            {
                                string[] tripdetails2 = m.Split(' ');
                                name.MilesDriven += Math.Round(Convert.ToDouble(tripdetails2[3]));
                                name.TotalHours += Convert.ToDateTime(tripdetails2[2]) - Convert.ToDateTime(tripdetails2[1]);
                                name.AverageSpeed = Convert.ToInt32(name.MilesDriven / (name.TotalHours.Hours));
                            }

                        }

                        //check if the average speed is greater than five and less than 100mph before displaying report
                        if(name.AverageSpeed > 5 && name.AverageSpeed < 100)
                        {
                            if(name.MilesDriven >0)
                            Console.WriteLine($"{name.DriverName}: {name.MilesDriven} miles @ {name.AverageSpeed}  mph");
                            else
                                Console.WriteLine($"{name.DriverName} :{name.MilesDriven} miles");

                            
                        }
                        //if the average speed is less than 5 or greater than 100mph, display a message 
                        else
                        {
                            Console.WriteLine($"{name.DriverName}: kindly note that your average speed was either below or above the required Limit");
                        }
                       
                    }
                     Console.ReadLine();
                }
                

            }
            while (UserInput.ToLower() == "yes");


        }
    }
}
